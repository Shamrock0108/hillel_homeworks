package com.hillel.java.homework5.figures;

import com.hillel.java.homework5.figures.FigureMethods;

public class Square implements FigureMethods {
    private double length;


    public Square(double length) {
        this.length = length;
    }


    @Override
    public double figureArea() {
        return  length * length;


    }

}
