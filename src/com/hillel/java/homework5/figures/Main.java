package com.hillel.java.homework5.figures;

import java.nio.file.Path;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Circle circle = new Circle(3.14, 34);
        Square square = new Square(34);
        Triangle triangle = new Triangle(23.32, 32);
        FigureMethods[] f = new FigureMethods[3];
        f[0] = circle;
        f[1] = square;
        f[2] = triangle;
        for(int i = 0;i<f.length;i++){
            System.out.println("Area " + i +": "+ f[i].figureArea());
        }
        System.out.println("The sum of the area is: ");
        suma(circle, square, triangle);

    }

    public static void suma(Circle circle, Square square, Triangle triangle) {
        System.out.println( circle.figureArea() + square.figureArea() + triangle.figureArea());

    }

}
