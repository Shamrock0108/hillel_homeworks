package com.hillel.java.homework5.figures;

public interface FigureMethods {

    double figureArea();

}
