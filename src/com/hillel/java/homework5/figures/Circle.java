package com.hillel.java.homework5.figures;

public class Circle implements FigureMethods {
    private  double Pi;
    private double radius;

    public Circle(double Pi, double radius) {
        this.Pi = Pi;
        this.radius = radius;
    }

    @Override
    public double figureArea() {
         return  Pi*Math.pow(radius,2);


    }


}
