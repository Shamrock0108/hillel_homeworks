package com.hillel.java.homework5.figures;

import com.hillel.java.homework5.figures.FigureMethods;

public class Triangle implements FigureMethods {
    private double height;
    private double width;
    private double b;

    public Triangle(double height, double width) {
        this.height = height;
        this.width = width;
    }
    public Triangle(){

    }

    @Override
    public double figureArea() {
         return (height + width)/2;

    }


}
