package com.hillel.java.homework5.Participants;

public class Human extends Participant{
    public Human(String name, int maxRange, int maxJump) {
        super(name, maxRange, maxJump);
    }
    @Override
    public boolean run(int treadmill) {
        if(treadmill <= getMaxRange()){
            System.out.println("---------------");
            System.out.println("Human " + getName() + " ran the distance " + treadmill + " meters,"
                    + " he ran " + getMaxRange());
            return true;
        }else{
            System.out.println("---------------");
            System.out.println("The Human " + getName() + " did not ran the distance " + treadmill + " meters," +
                    " run " + getMaxRange() + " meters");
            System.out.println(getName() + " Out of the competition");
            return false;

        }
    }

    @Override
    public boolean jump(int wall) {
        if(wall <= getMaxJump()){
            System.out.println("---------------");
            System.out.println("Human " + getName() + "  jumped over the wall " + wall + " meters,"
                    + " he jumped " + getMaxJump());
            return true;
        }else{
            System.out.println("---------------");
            System.out.println("Human " + getName() + " didn't jump over the wall  " + wall+ " meters," +
                    " he jumped" + getMaxJump());
            System.out.println(getName() + " Out of the competition");
            return false;
        }

    }




}
