package com.hillel.java.homework5.Participants;

public interface Let {

     boolean overcome(Participant pr);
}
