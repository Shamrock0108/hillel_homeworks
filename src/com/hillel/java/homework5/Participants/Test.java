package com.hillel.java.homework5.Participants;

public class Test {
    public static void main(String[] args) {
        Treadmill treadmill = new Treadmill();
        Wall wall = new Wall();
        Participant[] par1 = {new Cat("Smetana", 235, 10),
                new Human("Ivan", 234, 10),
                new Robot("Irobot", 200, 10)};
        Participant[] par2 = {new Cat("Vasya", 300, 30),
                new Human("Kyzmin", 400, 50),
                new Robot("Ijump", 200, 1000)};
        for (int i = 0; i < par1.length; i++) {
            treadmill.overcome(par1[i]);
            wall.overcome(par1[i]);
        }
        for (int i = 0; i < par2.length; i++) {
         treadmill.overcome(par2[i]);
         wall.overcome(par2[i]);
        }
    }
}


