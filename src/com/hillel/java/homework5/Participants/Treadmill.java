package com.hillel.java.homework5.Participants;

public class Treadmill implements Let{
    private int range = 300;
    @Override
    public boolean overcome(Participant pr) {
           return pr.run(range);
    }
}
