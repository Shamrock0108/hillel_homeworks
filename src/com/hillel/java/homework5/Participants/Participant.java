package com.hillel.java.homework5.Participants;

public abstract class Participant {
    private String name;
    private int maxRange;
    private int maxJump;

    public Participant(String name, int maxRange, int maxJump) {
        this.name = name;
        this.maxRange = maxRange;
        this.maxJump = maxJump;
    }

    protected Participant() {
    }

    public String getName() {
        return name;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public int getMaxJump() {
        return maxJump;
    }

    public abstract boolean run(int  maxRange);

    public abstract boolean jump(int maxJump);
}
