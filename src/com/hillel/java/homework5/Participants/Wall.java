package com.hillel.java.homework5.Participants;

public class Wall  implements Let{
    private int height = 40;


    @Override
    public boolean overcome(Participant pr) {
        return pr.jump(height);
    }


}
