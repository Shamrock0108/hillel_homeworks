package com.hillel.java.homework4;

public class Cat extends Animals{
    private  int maxRunDistance = 200 ;

private static int count = 0;
    public Cat(String name) {
        super(name);
        count++;
        System.out.println("Cat Number: " + count);
    }
    public void run(int distance) {
        if (distance >= maxRunDistance) {
            System.out.println( getName() + " is tired, can no longer run");
        } else if (distance <= 0) {
            System.out.println( getName() + " did not run," + getName() + " is resting");
        } else {
            System.out.println(getName() + " " + "Run" + " " + distance + " meters");
        }

    }

    @Override
    public void swim(int distance) {
        System.out.println("Cats don't swim");
    }


}
