package com.hillel.java.homework4;

public abstract class Animals {

    private String name;
    public static int count = 0;

    public Animals(String name) {
        System.out.println("---------------");
        this.name = name;


    }

    public abstract void run(int distance) ;

    public abstract void swim(int distance);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
