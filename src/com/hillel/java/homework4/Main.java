package com.hillel.java.homework4;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Vasya");
        cat.run(123);
        Dog dog = new Dog("Bobik");
        dog.swim(1);
        Cat cat1 = new Cat("Smetana");
        cat.swim(1);
        Dog dog1 = new Dog("Flash");
        dog1.run(500);

    }
}
