package com.hillel.java.homework4;

public class Dog extends Animals{
    private  int maxRunDistance = 500;
    private  int maxSwimDistance = 10;
    private static int count = 0;
    public Dog(String name) {
        super(name);
        count++;
        System.out.println("Dog number:" + count);
    }

    @Override
    public void run(int distance) {
        if (distance >= maxRunDistance) {
            System.out.println( getName() + " is tired, can no longer run");
        } else if (distance <= 0) {
            System.out.println( getName() + " did not run," + getName() + " is resting");
        } else {
            System.out.println(getName() + " " + "Run" + " " + distance + " meters");
        }
    }

    public void swim(int distance) {
        if(distance >= maxSwimDistance ) {
            System.out.println( getName() + " is tired, can no longer swim");
        } else if (distance <= 0) {
            System.out.println( getName() + " did not swim," + getName() + " is resting");
        }else {
            System.out.println(getName() + " " + "Swim" + " " + distance + " meters");

        }

    }


}
