package com.hillel.java.homework3;

import com.hillel.java.homework3.Employees1.SameName;

public class Main {
    public static void main(String[] args) {
        SameName sameName = new SameName("Igor");
        com.hillel.java.homework3.Employees2.SameName sameName1 = new com.hillel.java.homework3.Employees2.SameName("Igor");
        //Очень не удобно и непонятно
        Employees employees1 = new Employees("Vasya", "Slesar", "vasya@mail.com",
                05077777, 51);
        Car car = new Car("BMW", 2023, "Black");
        if (sameName.getName().equals(sameName1.getName())) {
            System.out.println("Error");
        }
        System.out.println("--------------");

        System.out.println(car.getName());
        car.start();
        System.out.println("----------------");
        System.out.println(employees1.getName());
        System.out.println("-----------------");
        System.out.println(employees1.toString());
    }
}
