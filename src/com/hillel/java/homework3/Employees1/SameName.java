package com.hillel.java.homework3.Employees1;

public class SameName {
    private  String name;

    public SameName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
