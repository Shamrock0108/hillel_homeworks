package com.hillel.java.homework3;

public class Car {
    private String name;
    private  int yearOfProduct;
    private String color;

    public Car(String name, int yearOfProduct, String color) {
        this.name = name;
        this.yearOfProduct = yearOfProduct;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void start() {
        startElectricity();
        startCommand();
        startFuelSystem();


    }


    private void  startElectricity() {
        System.out.println( "Start Electricity ");

    }
    private void startCommand(){
        System.out.println( "Say banzai");

    }

    private void startFuelSystem(){
        System.out.println("Engine start");

    }

    @Override
    public String toString() {
        return "HomeWork3.Car{" +
                "name='" + name + '\'' +
                ", yearOfProduct=" + yearOfProduct +
                ", color='" + color + '\'' +
                '}';
    }
}
