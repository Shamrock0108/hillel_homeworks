package com.hillel.java.homework8;

public class Methods {

    public static int doCalc(String[][] strings) throws MyArraySizeException, MyArrayDataException {
        if (strings.length != 4 && strings[0].length != 4) {
            System.out.println("Array length correct");
            throw new MyArraySizeException("Array length is wrong");
        }
        int sum = 0;
        try {
            for (int i = 0; i < strings.length; i++) {
                for (int j = 0; j < strings[i].length; j++) {
                    int a = Integer.parseInt(strings[i][j]);
                    sum += a;
                    System.out.print(a + " ");
                }
                System.out.print(" = " + sum);
                System.out.println();
            }

        } catch (NegativeArraySizeException e) {
            e.printStackTrace();
        }
        return sum;
    }
}
