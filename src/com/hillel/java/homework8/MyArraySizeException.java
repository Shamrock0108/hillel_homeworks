package com.hillel.java.homework8;

public class MyArraySizeException extends Exception{
    public MyArraySizeException(String message) {
        super(message);
    }
}
