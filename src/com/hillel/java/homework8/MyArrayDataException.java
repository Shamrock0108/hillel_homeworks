package com.hillel.java.homework8;

public class MyArrayDataException extends Exception{
    public MyArrayDataException(String message) {
        super(message);
    }
}
