package com.hillel.java.homework6;

public class main {
    public static void main(String[] args) {
        Methods methods = new Methods();
        methods.printThreeWords();
        methods.checkSumSign();
        methods.printColor();
        methods.compareNumbers();
        methods.checkNumbers(11,2);
        methods.additionCheck(-3);
        methods.positiveNegative(4);
        methods.rowAndNumber("Hi ",3);
        methods.leapYear(2023);
    }
}
