package com.hillel.java.homework7;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class StringMethods {
    String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot",
            "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive",
            " pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};

    public void go() {
        System.out.println("Guess the word: " + Arrays.toString(words));
        Random random = new Random();
        String randomWords = words[random.nextInt(words.length)];
         int count = 0;
         int hintCharacter = 2;
        boolean y = false;
        while (!y) {
            Scanner scn = new Scanner(System.in);
            String str = scn.nextLine();
            count++;

            if (randomWords.equalsIgnoreCase(str)) {
                System.out.println("You guessed the word: " + randomWords);
                System.out.println("Attempts used: " + count);
                break;
            }
            StringBuilder stringBuilder = new StringBuilder (randomWords.substring(0, hintCharacter));
            System.out.println(stringBuilder.append("###############"));
            System.out.println("You didn't guess the word, try again!");
            System.out.println("Attempts used: " + count);
            if (hintCharacter + 2 <= randomWords.length()) {
                hintCharacter += 2;
            }
        }


    }

    public void findSymbolOccurance(String a, char b) {
        int count = 0;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == b) {
                count++;
                System.out.println(count);
            }
        }
    }

    public void findWordPosition(String source, String target) {
        System.out.println("Source: " + source + "\nTarget: " + target);
        System.out.println("Result: " + source.indexOf(target));

    }


    public void stringReverse(String reverse) {
        reverse = new StringBuilder(reverse).reverse().toString();
        System.out.println(reverse);

    }

    public boolean isPalindrome(String word) {
        int length = word.length();
        for (int i = 0; i < (length / 2); i++) {
            if (word.charAt(i) != word.charAt(length - i - 1)) {
                System.out.println(word + " -> " + false);
                return false;
            } else {
                System.out.println(word + " -> " + true);

            }
        }
        return true;

    }
}
